using System;
using Xunit;

namespace ShapesLibrary.Tests
{
    public class CircleUnitTest
    {
        [Fact]
        public void UnitTest1() //�������� ������� �����.������ = ����� ������������� �����.
        {
            Circle circle = new Circle(5);
            Assert.Equal(25 * Math.PI, circle.GetArea());
        }

        [Fact]
        public void UnitTest2() //�������� ������� �����.������ = ������� ������������� �����.
        {
            Circle circle = new Circle(1.5);
            Assert.Equal(2.25 * Math.PI, circle.GetArea());
        }

        [Fact]
        public void UnitTest3() //������ = ������������� �����.
        {
            Assert.Throws<Exception>(() => new Circle(-5));
        }

        [Fact]
        public void UnitTest4() //�������� ����� ����������.
        {
            Circle circle = new Circle(5);
            Assert.Equal(10 * Math.PI, circle.GetPerimeter());
        }
    }
}