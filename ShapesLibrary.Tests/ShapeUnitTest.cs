using System;
using Xunit;

namespace ShapesLibrary.Tests
{
    public class ShapeUnitTest
    {
        [Fact]
        public void UnitTest1() // �������� ������� �����.
        {
            Circle circle = new Circle(5);
            Assert.Equal(25 * Math.PI, Shape.GetAreaOfUnknownShape(circle));
        }

        [Fact]
        public void UnitTest2() // �������� ������� ������������.
        {
            Triangle triangle = new Triangle(5, 2, 4);
            Assert.Equal(Math.Sqrt(14.4375), Shape.GetAreaOfUnknownShape(triangle));
        }
    }
}