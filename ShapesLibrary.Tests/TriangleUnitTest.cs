using System;
using Xunit;

namespace ShapesLibrary.Tests
{
    public class TriangleUnitTest
    {
        [Fact]
        public void UnitTest1() // �������� ������������� �������������.
        {
            Assert.Throws<Exception>(() => new Triangle(5, 2, 8));
        }

        [Fact]
        public void UnitTest2() // �������� ������������� �������������.
        {
            Assert.Throws<Exception>(() => new Triangle(5, 2, 0));
        }

        [Fact]
        public void UnitTest3() //�������� ������� ������������.
        {
            Triangle triangle = new Triangle(5, 2, 4);
            Assert.Equal(Math.Sqrt(14.4375), triangle.GetArea());
        }

        [Fact]
        public void UnitTest4() //�������� ��������� ������������.
        {
            Triangle triangle = new Triangle(5, 2, 4);
            Assert.Equal(11, triangle.GetPerimeter());
        }
    }
}