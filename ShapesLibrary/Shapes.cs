﻿using System;

namespace ShapesLibrary
{
    public abstract class Shape
    {
        public double Area { get; set; }

        public abstract double GetArea();

        public abstract double GetPerimeter();

        public static double GetAreaOfUnknownShape(Shape Shape) => Shape.GetArea();
    }

    public class Circle : Shape
    {
        public double Radius { get; set; }

        public Circle(double Radius)
        {
            if (Radius<=0)
                throw new Exception("Некорректный радиус");
            this.Radius = Radius;
        }

        public override double GetArea() => Math.PI * Math.Pow(Radius, 2);

        public override double GetPerimeter() => 2 * Math.PI * Radius;
    }

    public class Triangle : Shape
    {
        public double SideA { get; set; }
        public double SideB { get; set; }
        public double SideC { get; set; }

        public Triangle(double SideA, double SideB, double SideC)
        {
            if (SideA <= 0 || SideB <= 0 || SideC <= 0)
                throw new Exception("Введены некорректные длины сторон");
            if (!(SideA + SideB > SideC && SideA + SideC > SideB && SideB + SideC > SideA))
                throw new Exception("Треугольника с такими сторонами не существует");
            this.SideA = SideA;
            this.SideB = SideB;
            this.SideC = SideC;
        }

        public bool IsRectangular()
        {
            return Math.Pow(SideA, 2) == Math.Pow(SideB, 2) + Math.Pow(SideC, 2) ||
                Math.Pow(SideB, 2) == Math.Pow(SideA, 2) + Math.Pow(SideC, 2) ||
                Math.Pow(SideC, 2) == Math.Pow(SideA, 2) + Math.Pow(SideB, 2);
        }

        public override double GetArea()
        {
            double semiPerimeter = GetPerimeter() / 2;
            return Math.Sqrt(semiPerimeter * (semiPerimeter - SideA) * (semiPerimeter - SideB) * (semiPerimeter - SideC));
        }

        public override double GetPerimeter() => SideA + SideB + SideC;
    }
}